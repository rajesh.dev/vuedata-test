<?php

namespace VueData\Test\Model;

use Magento\Framework\Model\AbstractModel;
use VueData\Test\Model\ResourceModel\Customer as ResourceModel;

class Customer extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
