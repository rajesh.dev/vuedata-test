<?php

namespace VueData\Test\Model\Api;

class OrderSave
{
    /**
     * @var \VueData\Test\Model\CustomerFactory
     */
    private $CustomModel;
    /**
     * @var \VueData\Test\Model\ResourceModel\Customer
     */
    private $CustomResourceModel;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @param \VueData\Test\Model\CustomerFactory $CustomModel
     * @param \VueData\Test\Model\ResourceModel\Customer $CustomResourceModel
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \VueData\Test\Model\CustomerFactory $CustomModel,
        \VueData\Test\Model\ResourceModel\Customer $CustomResourceModel,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->CustomModel = $CustomModel;
        $this->CustomResourceModel = $CustomResourceModel;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */

    public function save(int $orderId, string $customerName, int $customerId, string $status)
    {
        try {
            $model = $this->CustomModel->create();
            $model->setOrderId($orderId);
            $model->setCustomerName($customerName);
            $model->setCustomerId($customerId);
            $model->setOrderStatus($status);
            $this->CustomResourceModel->save($model);
            return 'Order details added successfully';
        }
        catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
        }
    }
}
