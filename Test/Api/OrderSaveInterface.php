<?php

namespace VueData\Test\Api;

/**
 * Interface OrderSaveInterface
 * @api
 */
interface OrderSaveInterface{
    /**
    * GET for Post api
     * @param int $orderId
     * @param string $customerName
     * @param int $customerId
     * @param string $status
    * @return string
    */
    public function save(int $orderId, string $customerName, int $customerId, string $status);
}
